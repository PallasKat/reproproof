#!/bin/bash

echo "CPU EXEC"
ftn -hnoacc repro.f90 -o cpu.out 
srun -n 1 --partition=debug --gres=gpu:1 -t 00:10:00 ./cpu.out 
mv results.txt cpu_res.txt
mv results.bin cpu_res.bin

echo "GPU EXEC"
ftn -hacc repro.f90 -o gpu.out
srun -n 1 --partition=debug --gres=gpu:1 -t 00:10:00 ./gpu.out 
mv results.txt gpu_res.txt
mv results.bin gpu_res.bin

diff cpu_res.bin gpu_res.bin

