module libfct
contains
  pure double precision function fact(x) result(y)
    !$acc routine seq
    integer, intent(in) :: x
    integer :: i
    y = 1.0
    do i=1,x
      y = y*i
    end do
  end function fact

  pure double precision function ipow(x, n) result(y)
    !$acc routine seq
    integer, intent(in) :: n
    double precision, intent(in) :: x
    integer :: i
    y = x
    if (n > 1) then
      do i=2,n
        y = y*x
      end do
    endif
  end function ipow
end module libfct  

program repro
  use libfct

  implicit none
  
  integer, parameter :: N = 1000, fid=1204, bid=1980, verbose=0
  double precision, parameter :: a = -3.14, b = 3.14

  integer :: i, j
  double precision :: x, imgSum, delta

  double precision, dimension(N) :: preimg, img

  !double precision, dimension(:), allocatable :: preimg, img
  !allocate(img(N))
  !allocate(preimg(N))
  
  if (N-1 .le. 0) then
    delta = abs(b - a)
  else
    delta = abs(b - a)/(N-1)
  end if

  x = a
  if (verbose .eq. 1) then
    print *, "a", a
    print *, "x", x
    print *, "N", N
    print *, "delta", delta
    print *, "ramge", abs(b - a)
  end if

  do i=1,N
    preimg(i) = x
    x = x + delta
  end do

!  if (verbose .eq. 1) then
!    print *, "x", x
!    print *, "preimg", preimg
!    print *, "img", img
!  end if
  
  !$acc data copy(preimg, img)
  !$acc parallel private(x)
  !$acc loop
  do i=1,N
    x = preimg(i)
    !img(i) = 1.0 - preimg(i)*preimg(i)/2.0 + ipow(preimg(i),4)/fact(4) - ipow(preimg(i),6)/fact(6) + ipow(preimg(i),8)/fact(8)
    img(i) = 1.0 - x*x/2.0 + ipow(x,4)/fact(4) - ipow(x,6)/fact(6) + ipow(x,8)/fact(8)
    
    if (verbose .eq. 2) then
    !  print *, "x", x
    !  print *, "f1.", ipow(x,1), fact(1), ipow(x,1)/fact(1)
    !  print *, "f2.", x*x, x*x/2.0
    !  print *, "f4.", ipow(x,4), fact(4), ipow(x,4)/fact(4)
    !  print *, "f6.", ipow(x,6), fact(6), ipow(x,6)/fact(6)
    !  print *, "f8.", ipow(x,8), fact(8), ipow(x,8)/fact(8)
    !  print *, "s2.", 1.0 - x*x/2.0
    !  print *, "s3.", 1.0 - x*x/2.0 + ipow(x,4)/fact(4)
    !  print *, "s4.", 1.0 - x*x/2.0 + ipow(x,4)/fact(4) - ipow(x,6)/fact(6)
    !  print *, "s5.", 1.0 - x*x/2.0 + ipow(x,4)/fact(4) - ipow(x,6)/fact(6) + ipow(x,8)/fact(8)
    end if
  end do
  !$acc end parallel
  !$acc end data

  if (verbose .eq. 1) then
    print *, "preimg", preimg
    print *, "img", img
  end if
  
  imgSum = 0.0
  do i=1,N
    imgSum = imgSum + img(i)
  end do

  print *, "sum", imgSum

  ! write results to a text file
  open (unit=fid, file="results.txt", action="write", status="replace")
  do i=1,N
    write (fid,*) preimg(i), " ", img(i)
  end do
  write (fid,*) N, " ", imgSum
  close(fid)

  open (unit=bid, file="results.bin", form="unformatted")
  write (bid) img
  close(bid)

  !deallocate(preimg)
  !deallocate(img)
  
end program repro

