#!/bin/bash

# --------------------------------------------------------
# Usage of the script
# --------------------------------------------------------
usage() {
cat << EOF
Usage: ${0##*/} -c COMPILER

Test the reproducibility of a simple code.

Arguments to provide:
  -c the compiler to use (e.g. cray, gnu or pgi)

EOF
}

compiler=OFF

# --------------------------------------------------------
# Management of the provided "options"
# --------------------------------------------------------
while getopts hc: opt; do
  case $opt in
  h) # -h option: asking for help, exit is normal
      usage
      exit 0
      ;;
  c) # the compiler
      compiler=$OPTARG
      ;;
  \?) # unrecognized option: show help
      usage
      exit 1
      ;;
  esac
done

if [ "${compiler}" == "OFF" ] ; then
   echo "[ERROR] no compiler provided (no default)"
   exit 1
fi

craymodules() {
  module purge    
  module load craype-haswell
  module load craype-accel-nvidia35
  module load PrgEnv-cray/15.10_cuda_7.0
  module load cmake/3.1.3
  module swap cce/8.4.0a
  module load cray-libsci_acc/3.3.0
  module load GCC/4.9.3-binutils-2.25
}

pgimodules() {
  module purge    
  module load craype-haswell
  module load GCC/4.9.3-binutils-2.25
  module load PrgEnv-pgi/16.3
  module load GCC/4.9.3-binutils-2.25
}

runcpu() {
  srun -n 1 --partition=debug --gres=gpu:1 -t 00:10:00 ./cpu.out 
  mv results.txt cpu_res.txt
  mv results.bin cpu_res_pgi.bin
}

rungpu() {
  srun -n 1 --partition=debug --gres=gpu:1 -t 00:10:00 ./gpu.out 
  mv results.txt gpu_res.txt
  mv results.bin gpu_res_pgi.bin
}

# --------------------------------------------------------
# Main driver and switch between compilers
# --------------------------------------------------------
case "${compiler}" in
  cray )
    echo "CRAY MODULES LOAD"
    craymodules
    
    echo "CPU EXEC"
    ftn -hnoacc -hadd_paren -hfp0 -hflex_mp=intolerant repro.f90 -o cpu.out
    #ftn -hnoacc repro.f90 -o cpu.out  
    runcpu
    
    echo "GPU EXEC"
    ftn -hacc -hadd_paren -hfp0 -hflex_mp=intolerant repro.f90 -o gpu.out
    #ftn -hacc repro.f90 -o gpu.out
    rungpu
    ;;
    
  pgi )
    echo "PGI MODULES LOAD"
    pgimodules
    
    echo "CPU EXEC"
    pgfortran repro.f90 -o cpu.out
    runcpu
    
    echo "GPU EXEC"
    #pgfortran -acc=verystrict -Minfo -Kieee -ta=nvidia,cc35 -Minline repro.f90 -o gpu.out
    #pgfortran -Minfo  -acc=verystrict -Kieee -ta=nvidia,cc35,nollvm,keepgpu,keepptx repro.f90 -o gpu.out
    pgfortran -Minfo -acc -ta=nvidia,cc35 repro.f90 -o gpu.out
    rungpu
    ;;
    
  * )
    echo "[ERROR] ${compiler} is an unsupported compiler"
    exit 1
  esac
   
diff cpu_res.bin gpu_res.bin


