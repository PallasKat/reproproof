# README #

### What is this repository for? ###

* The purpose of this repository is to provide a small demo of a code that can be make reproductible with the Cray compiler with and without accelerator
* Version 0.1

### How do I get set up? ###

* Clone the master branch
* The script `duo.sh` and `duo_fail.sh` illustrate how to compile and execute the code
* The `duo_fail` should produce a reproductible code on the accelerator
* The `duo_fail.sh` should not produce a reproductible code on the accelerator

On Kesch one should load the correct modules:

```
module load craype-haswell
module load craype-accel-nvidia35
module load PrgEnv-cray/15.10_cuda_7.0
module load cmake/3.1.3
module swap cce/8.4.0a
module load cray-libsci_acc/3.3.0
module load GCC/4.9.3-binutils-2.25
```
Before loading these modules, an implicit `module purge` is assumed.

### Example of execution ###
The first example show that one execution without accelerator (CPU EXEC) and one with accelerator (GPU EXEC) produce the same computation. The key flags to achieve this results are:

* `-hadd_paren` to ensure left to right evaluation of mathematical expression
* `-hfp0` to avoid usage of floating point contraction operations, like the FMA
* `-hflex_mp=intolerant` to control optimizations which may affect fp repeatability and `intolerant` has the highest probability of repeatable results

The execution of the script produces the following output on Kesch:

```
> ./duo.sh 
CPU EXEC
 N 1000
 sum 1.93317440511974867E+124
GPU EXEC
 N 1000
 sum 1.93317440511974867E+124
```

The second example show that without the correct flags the execution is not reproductible on the accelerator:
```
> ./duo_fail.sh 
CPU EXEC
 N 1000
 sum 1.93317440512256763E+124
GPU EXEC
 N 1000
 sum 1.9331744051225681E+124
Binary files cpu_res.bin and gpu_res.bin differ
```

### Execution on host or device ? ###
The executable compiled with accelerator (`gpu.out` in the scripts) produces with `nvprof` the following output:

```
> srun -n 1 --partition=debug --gres=gpu:1 -t 00:10:00 nvprof ./gpu.out
==91463== NVPROF is profiling process 91463, command: ./gpu.out
==91463== Profiling application: ./gpu.out
==91463== Profiling result:
Time(%)      Time     Calls       Avg       Min       Max  Name
 41.79%  11.488us         1  11.488us  11.488us  11.488us  repro_$ck_L35_1
 34.34%  9.4400us         2  4.7200us  4.3840us  5.0560us  [CUDA memcpy DtoH]
 23.86%  6.5600us         2  3.2800us  3.2640us  3.2960us  [CUDA memcpy HtoD]

==91463== API calls:
Time(%)      Time     Calls       Avg       Min       Max  Name
 99.90%  1.43360s         1  1.43360s  1.43360s  1.43360s  cuCtxCreate
  0.06%  803.52us         1  803.52us  803.52us  803.52us  cuMemHostAlloc
  0.02%  346.46us         2  173.23us  6.8230us  339.64us  cuMemAlloc
  0.01%  140.36us         1  140.36us  140.36us  140.36us  cuModuleLoadData
  0.00%  66.131us         2  33.065us  29.344us  36.787us  cuMemcpyDtoH
  0.00%  26.528us         2  13.264us  8.2140us  18.314us  cuMemcpyHtoD
  0.00%  19.388us         1  19.388us  19.388us  19.388us  cuStreamCreate
  0.00%  17.685us         1  17.685us  17.685us  17.685us  cuLaunchKernel
  0.00%  14.141us        34     415ns     301ns  1.7610us  cuEventCreate
  0.00%  3.1210us         1  3.1210us  3.1210us  3.1210us  cuStreamSynchronize
  0.00%  1.3850us         2     692ns     551ns     834ns  cuDeviceGetCount
  0.00%     955ns         1     955ns     955ns     955ns  cuMemHostGetDevicePointer
  0.00%     686ns         2     343ns     180ns     506ns  cuFuncGetAttribute
  0.00%     608ns         1     608ns     608ns     608ns  cuModuleGetFunction
  0.00%     598ns         3     199ns     169ns     235ns  cuDeviceGetAttribute
  0.00%     530ns         1     530ns     530ns     530ns  cuCtxSetCurrent
  0.00%     495ns         1     495ns     495ns     495ns  cuModuleGetGlobal
  0.00%     481ns         2     240ns     222ns     259ns  cuDeviceGet
  0.00%     446ns         1     446ns     446ns     446ns  cuFuncSetCacheConfig
  0.00%     215ns         1     215ns     215ns     215ns  cuCtxGetCurrent
 N 1000
 sum 1.93317440511974867E+124
```
and the one without accelerator (`cpu.out` in the scripts) produces:

```
> srun -n 1 --partition=debug --gres=gpu:1 -t 00:10:00 nvprof ./cpu.out
 N 1000
 sum 1.93317440511974867E+124
======== Warning: No CUDA application was profiled, exiting
```

### Contribution guidelines ###

* Code review by Christophe C. and Andrea A.

### Who do I talk to? ###

* Repo owner